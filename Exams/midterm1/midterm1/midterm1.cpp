#include <iostream>
#include <time.h>
using namespace std;

int main()
{
	//variables

	int userHp;
	int aiHp;
	int userDmg;
	int aiDmg;
	int userMove;
	int aiMove;
	int userMinDmg;
	int aiMinDmg;
	int userMaxDmg;
	int aiMaxDmg;

	//input damage and hp

	cout << "Please input minimum damage for player: ";
	cin >> userMinDmg;

	cout << "Input maximum damage for player: ";
	cin >> userMaxDmg;

	cout << "Input minimum damage for AI: ";
	cin >> aiMinDmg;

	cout << "Input maximum damage for AI: ";
	cin >> aiMaxDmg;

	cout << "Input health for player: ";
	cin >> userHp;

	cout << "Input health for AI: ";
	cin >> aiHp;

	//loop and conditions with their outputs

	while (true)
	{

		srand(time(NULL));
		aiMove = rand() % 3 + 1;
		userDmg = rand() % (userMaxDmg - userMinDmg) + userMinDmg;
		aiDmg = rand() % (aiMaxDmg - aiMinDmg) + aiMinDmg;


		cout << "Choose your move. 1 for ATTACK, 2 for DEFEND, 3 for WILD ATTACK: ";
		cin >> userMove;



		if (userMove < 1 || userMove > 3)
		{
			cout << "Move is invalid." << endl;
		}

		else if (userMove == 1 && aiMove == 1)
		{
			cout << "Player chose ATTACK." << endl;
			cout << "Enemy chose ATTACK." << endl;

			userHp = userHp - (aiDmg * 1);
			aiHp = aiHp - (userDmg * 1);

			cout << "Both receive 100% damage." << endl;
		}

		else if (userMove == 2 && aiMove == 2)
		{
			cout << "Both players chose to DEFEND. Nothing happens." << endl;
		}

		else if (userMove == 3 && aiMove == 3)
		{
			cout << "Player chose WILD ATTACK." << endl;
			cout << "Enemy chose WILD ATTACK." << endl;

			userHp = userHp - (aiDmg * 2);
			aiHp = aiHp - (userDmg * 2);

			cout << "Both receive 200% damage." << endl;
		}

		else if (userMove == 1 && aiMove == 2)
		{
			cout << "Player chose ATTACK." << endl;
			cout << "Enemy chose DEFEND." << endl;

			aiHp = aiHp - (userDmg * .5);

			cout << "Enemy receives 50% " << endl;
		}

		else if (userMove == 1 && aiMove == 3)
		{
			cout << "Player chose ATTACK." << endl;
			cout << "Enemy chose WILD ATTACK." << endl;

			userHp = userHp - (aiDmg * 2);
			aiHp = aiHp - (userDmg * 1);

			cout << "Player receives 200% damage." << endl;
			cout << "Enemy receives 100 % damage." << endl;
		}

		else if (userMove == 2 && aiMove == 1)
		{
			cout << "Player chose DEFEND." << endl;
			cout << "Enemy chose ATTACK." << endl;

			userHp = userHp - (aiDmg * .5);

			cout << "Player receives 50% damage." << endl;
		}

		else if (userMove == 2 && aiMove == 3)
		{
			cout << "Player chose DEFEND." << endl;
			cout << "Enemy chose WILD ATTACK." << endl;

			aiHp = aiHp - (userDmg * 2);

			cout << "Attack countered. Enemy receives 200% damage." << endl;
		}

		else if (userMove == 3 && aiMove == 1)
		{
			cout << "Player chose WILD ATTACK." << endl;
			cout << "Enemy chose ATTACK." << endl;

			userHp = userHp - (aiDmg * 1);
			aiHp = aiHp - (userDmg * 2);

			cout << "Player receives 100% damage." << endl;
			cout << "Enemy receives 200 % damage." << endl;
		}

		else if (userMove == 3 && aiMove == 2)
		{
			cout << "Player chose WILD ATTACK." << endl;
			cout << "Enemy chose DEFEND." << endl;

			userHp = userHp - (aiDmg * 2);

			cout << "Attack countered. Player receives 200% damage." << endl;
		}

		cout << "User HP: " << userHp << endl;
		cout << "Enemy HP: " << aiHp << endl;

		if (userHp <= 0)
		{
			cout << "You died." << endl;
			break;
		}

		if (aiHp <= 0)
		{
			cout << "You win." << endl;
			break;
		}
	}



	system("pause");
	return 0;
}

