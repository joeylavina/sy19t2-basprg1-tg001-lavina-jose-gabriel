#include <iostream>
#include <string>


using namespace std;


int main()
{
	int nums[10] = { -20, 0, 7, 3, 2, 1, 8, -3, 6, 3 };

	//For the largest number (Check answer key???)
	int largestNum = INT_MIN;
	for (int i = 0; i < 10; i++)
	{
		if (nums[i] > largestNum)
		{
			largestNum = nums[i];
		}
	}

	//Output
	cout << "Largest number is " << largestNum << "!" << endl;

	system("pause");
	return 0;
}