#include<iostream>
#include<string>

using namespace std;

int main()
{
	string items[] = { "RedPotion", "BluePotion", "YggdrasilLeaf", "Elixir", "TeleportScroll", "RedPotion", "RedPotion", "Elixir" };
	int arraySize = 8;

	//Input
	string itemFind;
	cout << "Item to find: ";
	cin >> itemFind;

	//For loop and checking
	int indexItem = -1;
	for (int i = 0; i < arraySize; i++)
	{
		if (itemFind == items[i])
		{
			indexItem = i;
			break;
		}
	}

	//Output
	if (indexItem >= 0)
	{
		cout << itemFind << " found at index " << indexItem << "." << endl;
	}
	else
	{
		cout << itemFind << " does not exist" << "." << endl;
	}


	system("Pause");	
	return 0;
}