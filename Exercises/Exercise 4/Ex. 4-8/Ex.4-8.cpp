#include <iostream>
using namespace std;

int main() {

	//input

	int n;

	cout << "How many numbers do you have?" << endl; 
	cin >> n;

	int* array = new int[n]; 

	//values loop

	int count = 1;
	for (int i = 0; i < n; i++)
	{
		cout << "Input number (count " << count << "):" << endl;
		cin >> array[i];
		count = count + 1;
		
	}
	
	//finding the smallest value

	int lowest = 0;

	for (int i = 0; i < n - 1; i++)
	{
		for (int j = i + 1; j < n - 1; j++) 
		{
			if (array[i] > array[lowest]) 
			{
				lowest = i;
				array[i] = array[j];
				array[j] = lowest; 
			}

		}
	}

	//output

	cout << "Sorted array is: " << endl;
	int count2 = 1;
	for (int i = 0; i<n; i++) {
		cout << count2 << endl; 
		count2 = count2 + 1;
	}

	system("Pause");
	return 0;
}