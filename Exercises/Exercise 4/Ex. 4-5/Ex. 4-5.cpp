#include <iostream>
#include <string>

using namespace std;


int main()
{
	//string and array
	string items[] = { "RedPotion", "BluePotion", "YggdrasilLeaf", "Elixir", "TeleportScroll", "RedPotion", "RedPotion", "Elixir" };
	int arraySize = 8;

	//Input
	string itemFind;
	cout << "Item to find: ";
	cin >> itemFind;

	//counting instances 
	int count = 0;
	for (int i = 0; i < arraySize; i++)
	{
		if (itemFind == items[i])
		{
			count++;
		}
	}

	// Print result
	cout << "You have " << count << " " << itemFind << "." << endl;

	system("pause");
	return 0;
}
