#include <iostream>
#include <string>
#include <time.h>

using namespace std;


int main()
{
	srand(time(0));

	string items[] = { "RedPotion", "BluePotion", "YggdrasilLeaf", "Elixir", "TeleportScroll", "RedPotion", "RedPotion", "Elixir" };
	int arraySize = 8;

	//user input
	string itemFind;
	cout << "Item to find: ";
	cin >> itemFind;

	//Counter
	int count = 0;
	for (int i = 0; i < arraySize; i++)
	{
		if (itemFind == items[i])
		{
			count++;
		}
	}

	//Output
	cout << "You have " << count << " " << itemFind << endl;

	system("pause");
	return 0;
}
