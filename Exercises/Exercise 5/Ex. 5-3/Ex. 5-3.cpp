#include<iostream>
#include<string>
#include<time.h>


using namespace std;

void PrintArray() {
	string items[] = { "RedPotion", "BluePotion", "YggdrasilLeaf", "Elixir", "TeleportScroll", "RedPotion", "RedPotion", "Elixir" };
	int arraySize = 8;

	for (int i = 0; i < arraySize; i++)
	{
		cout << items[i] << endl;
	}


}

void ItemExists() {
	string items[] = { "RedPotion", "BluePotion", "YggdrasilLeaf", "Elixir", "TeleportScroll", "RedPotion", "RedPotion", "Elixir" };
	int arraySize = 8;

	
	string itemToFind;
	cout << "Input item to find: ";
	cin >> itemToFind;

	
	bool itemExists = false;
	for (int i = 0; i < arraySize; i++)
	{
		if (itemToFind == items[i])
		{
			itemExists = true;
			break;
		}
	}
	
	if (itemExists)
	{
		cout << itemToFind << " exists" << endl;
	}
	else
	{
		cout << itemToFind << " does not exist" << endl;
	}
}

void LargestNum()
{
	int numbers[10] = { -20, 0, 7, 3, 2, 1, 8, -3, 6, 3 };


	int largest = INT_MIN;
	for (int i = 0; i < 10; i++)
	{
		if (numbers[i] > largest)
		{
			largest = numbers[i];
		}
	}

	cout << "Largest number is " << largest << endl;
}

void linearSearch() {
	string items[] = { "RedPotion", "BluePotion", "YggdrasilLeaf", "Elixir", "TeleportScroll", "RedPotion", "RedPotion", "Elixir" };
	int arraySize = 8;

	
	string itemToFind;
	cout << "Input item to find: ";
	cin >> itemToFind;

	
	int itemAtIndex = -1;
	for (int i = 0; i < arraySize; i++)
	{
		if (itemToFind == items[i])
		{
			itemAtIndex = i;
			break;
		}
	}


	if (itemAtIndex >= 0)
	{
		cout << itemToFind << " found at index " << itemAtIndex << endl;
	}
	else
	{
		cout << itemToFind << " does not exist" << endl;
	}
}

void instances() {
	string items[] = { "RedPotion", "BluePotion", "YggdrasilLeaf", "Elixir", "TeleportScroll", "RedPotion", "RedPotion", "Elixir" };
	int arraySize = 8;

	
	string itemToFind;
	cout << "Input item to find: ";
	cin >> itemToFind;

	
	int count = 0;
	for (int i = 0; i < arraySize; i++)
	{
		if (itemToFind == items[i])
		{
			count++;
		}
	}

	
	cout << "You have " << count << " " << itemToFind << endl;
}

void addingArrays() {
	
	int numbers1[] = { 1, 2, 3, 4, 5 };
	int numbers2[] = { 0, 7, -10, 5, 3 };
	
	for (int i = 0; i < 5; i++) {
		numbers1[i] += numbers2[i];
	}

	cout << "The sum of the arrays is: ";
	for (int i = 0; i < 5; i++) cout << numbers1[i] << " " << endl;

}

void randomElement() {
	
	srand(time(0));

	string items[] = { "RedPotion", "BluePotion", "YggdrasilLeaf", "Elixir", "TeleportScroll", "RedPotion", "RedPotion", "Elixir" };
	int arraySize = 8;

	string itemFind;
	cout << "Input item to find: ";
	cin >> itemFind;

	
	int count = 0;
	for (int i = 0; i < arraySize; i++)
	{
		if (itemFind == items[i])
		{
			count++;
		}
	}

	
	cout << "You have " << count << " " << itemFind << endl;

}

void selectionSort() {

	int n;

	cout << "How many numbers do you have?" << endl;
	cin >> n;

	int* array = new int[n];


	int count = 1;
	for (int i = 0; i < n; i++)
	{
		cout << "Input number (count " << count << "):" << endl;
		cin >> array[i];
		count = count + 1;

	}


	int lowest = 0;

	for (int i = 0; i < n - 1; i++)
	{
		for (int j = i + 1; j < n - 1; j++)
		{
			if (array[i] > array[lowest])
			{
				lowest = i;
				array[i] = array[j];
				array[j] = lowest;
			}

		}
	}


	cout << "Sorted array is: " << endl;
	int count2 = 1;
	for (int i = 0; i<n; i++) {
		cout << count2 << endl;
		count2 = count2 + 1;
	}
}

void bubbleSort() {

		int cons;
		int array[10];

		cout << "Enter 10 numbers: " << endl;
		for (int i = 0; i<10; i++)
		{
			cin >> array[i];
		}
		
		cout << endl;
		cout << "You entered: " << endl;

		for (int j = 0; j<10; j++)
		{
			cout << array[j];
			cout << endl;
		}
		cout << endl;
		for (int i = 0; i<9; i++)
		{
			for (int j = 0; j<9; j++)
			{
				if (array[j]>array[j + 1])
				{
					cons = array[j];
					array[j] = array[j + 1];
					array[j + 1] = cons;
				}
			}
		}
	
		cout << "Sorted array is: " << endl;
		for (int i = 0; i<10; i++)
		{
			cout << array[i] << endl;
		}
}
	

	



int main() {


	PrintArray();
	system("Pause");

	ItemExists();
	system("Pause");

	LargestNum();
	system("Pause");

	linearSearch();
	system("Pause");

	instances();
	system("Pause");

	addingArrays();
	system("Pause");

	randomElement();
	system("Pause");

	selectionSort();
	system("Pause");

	bubbleSort();
	system("Pause");
	return 0;
}