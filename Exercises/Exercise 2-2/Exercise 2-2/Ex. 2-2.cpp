#include<iostream>;

using namespace std;

int main()
{

	//input

	int initialHp = 100;
	int actualHp = 0;

	cout << "Current HP: ";
	cin >> actualHp;



	//compute
	float percent = actualHp / initialHp;
	cout << actualHp << " / " << initialHp << " (" << percent * 100.0f << ")" << endl;

	//checking percentages
	if (percent == 1.0f)
	{
		cout << "Full" << endl;
	}
	else if (percent >= 0.5f)
	{
		cout << "Green" << endl;
	}
	else if (percent >= 0.2f && percent < 0.5f)
	{
		cout << "Yellow" << endl;
	}
	else if (percent <= 0.0f)
	{
		cout << "Dead" << endl;
	}






	system("Pause");
	return 0;
}