#include<iostream>
#include<conio.h>
#include<time.h>

using namespace std;

void printRepeating(int arr[], int size)
{
	int i, j;
	cout << "Repeating elements are: ";
	for (i = 0; i < size; i++)
		for (j = i + 1; j < size; j++)
			if (arr[i] == arr[j])
				cout << arr[i] << " " << endl;
}

int main()
{
	int array[20], i, largest = 0, secondLar = 0, pos1, pos2;
	int n;

	//Getting the random values
	for (i = 0; i < 20; ++i)
	{
		array[i] = (rand() % 100) + 1;

		cout << array[i] << endl;
	}
	//Finding Largest
	for (i = 0; i < 20; ++i)
	{
		if (array[i]>largest)
		{
			largest = array[i];
			pos1 = i;
		}
	}
	//finding second largset
	for (i = 0; i < 20; ++i)
	{
		if (array[i]>secondLar)
		{
			//Ignoring largest in order to get second largest
			if (array[i] == largest)
				continue;              
			secondLar = array[i];
			pos2 = i;
		}
	}
	//Print output
	cout << "Largest Number : " << largest << " at position " << (pos1 + 1) << endl;
	cout << "Second Largest Number: " << secondLar << " at position " << (pos2 + 1) << endl;


	printRepeating(array, 20);

	system("Pause");
	return 0;
}