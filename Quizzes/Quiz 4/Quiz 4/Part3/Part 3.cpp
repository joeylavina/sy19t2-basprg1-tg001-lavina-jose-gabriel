#include<iostream>
#include<string.h>

using namespace std;


int main()
{
	//input
	char string[50], temp;
	int j;
	cout << "Enter a string : ";
	cin >> string;

	//Reversing the string
	j = strlen(string) - 1;
	for (int i = 0; i < j; i++, j--)
	{
		temp = string[i];
		string[i] = string[j];
		string[j] = temp;
	}
	cout << "Reverse string : " << string << endl;

	system("Pause");
	return 0;
}